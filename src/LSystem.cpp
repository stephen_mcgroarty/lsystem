#include "LSystem.h"
#include <math.h>
#include <string>
#include <string.h>



static const glm::vec3 s_Colours[5] 
{
	{1.0f,0.0f,0.0f},
	{0.26f,0.833f,0.26f},
	{0.58f,0.29f,0.0f},
	{0.0f,0.0f,1.0f},
	{0.16f,0.633f,0.16f},


};


LSystem::LSystem(float angle,const std::string & axion)
				: m_Angle(angle),m_Step(0.25f),m_Axion(axion)
{ 
	m_Angle = m_Angle*(M_PI/180.0f);
}


void LSystem::AddProduction(char c,const std::string & str)
{
	m_Productions.insert(std::pair<char,std::string>(c,str));
}


void LSystem::ResolveGrammar(std::string & out,int iterations) const
{
	for (int iteration =0; iteration < iterations; ++iteration)
	{
		std::string temp { out };
		out = "";

		for (auto c : temp)
		{
			auto itr = m_Productions.find(c);
			if (itr != m_Productions.end())
			{
				out += itr->second;
			} 
			else 
			{
				char ch[2] = {c,'\0'};
				out = out + std::string(ch);
			}
		}
	}
	printf("Generated string of length %lu\n",out.size());
}




const char * LSystem::TurtleInterpret(const char * itr, 
							glm::vec2 turtlePosition, 
							glm::vec2 turtleDirection,
							glm::vec3 activeColour,
							float heading,
							std::shared_ptr<Mesh> pMesh) const
{
	while (*itr != '\0')
	{
		if (*itr == 'F' || *itr == 'A' || *itr == 'B')
		{
			pMesh->AddVertice(turtlePosition);
			pMesh->AddColour(activeColour);
			turtlePosition += turtleDirection*m_Step;
			pMesh->AddVertice(turtlePosition);
			pMesh->AddColour(activeColour);
		} 
			else if (*itr == 'f')
		{
			turtlePosition += turtleDirection*m_Step;
		} 
			else if (*itr == '-' || *itr == '+')
		{
			const float twoPI = 360.0f*(M_PI/180.0f);
			heading += m_Angle * (*itr=='+'?-1.0f:1.0f);

			if (heading < 0)
				heading = twoPI + heading;
			else if (heading > twoPI)
				heading = heading - twoPI;

			turtleDirection = glm::normalize( glm::vec2(cos(heading),-sin(heading)));
		} 
		else if (*itr == '[')
		{
			// Move the iterator to stop the next call hitting '['
			itr = TurtleInterpret(itr+1,turtlePosition,turtleDirection,activeColour,heading,pMesh);
		} 
		else if(*itr == ']')
		{
			return itr;
		}
		else if(*itr == 'c')
		{
			++itr;
			char ch[2] = {*itr,'\0'};
			int index = atoi(ch);
			activeColour = s_Colours[index];
		} 
		else
		{
			printf("Unknown patten %c\n",*itr);
		}

		/*printf("\tCommand %c Status: Position(%f,%f), heading(%f,%f)\n",*itr,
								turtlePosition.x,turtlePosition.y,
								turtleDirection.x,turtleDirection.y);*/
		++itr;
	}

	return itr;
}



std::shared_ptr<Mesh> LSystem::InterpretString(const std::string & axion) const
{
	std::shared_ptr<Mesh> mesh {new Mesh()};

	const glm::vec2 UP {0.0f,1.0f};
	const glm::vec2 ROTATION_ZERO {1.0f,0.0f};

	glm::vec2 turtlePosition {0.0f,0.0f};
	glm::vec2 turtleDirection{UP};
	float heading = 270.0f*(M_PI/180.0f);



	glm::vec3 activeColour = s_Colours[0];


	const char * itr = axion.c_str();
	TurtleInterpret(itr,turtlePosition,turtleDirection,activeColour,heading,mesh);

	return mesh;
}


std::shared_ptr<Mesh> LSystem::Run(int iterations)
{
	std::string axion {m_Axion};

	ResolveGrammar(axion,iterations);

	std::shared_ptr<Mesh> mesh = InterpretString(axion);

	mesh->NormalizeToScreen();
	mesh->Finalize();
	return mesh;
}
