#include "Application.h"
#include "LSystem.h"

Application::Application()
{
	m_pRenderer = std::unique_ptr<Renderer>(new Renderer(800,800));

	m_pProgram = std::shared_ptr<ShaderProgram>(new ShaderProgram);
	m_pProgram->LoadShader(ShaderProgram::VERTEX,"../Resources/2DSimple.vert");
	m_pProgram->LoadShader(ShaderProgram::FRAGMENT,"../Resources/2DSimple.frag");
	m_pProgram->Finalize();

	m_Projection = glm::perspective(45.0f,800.0f/800.0f,0.1f,4000.0f);


	//LSystem lSystem(90.0f,{"F-F-F-F"});

	//lSystem.AddProduction('A',"A+B++B-A--AA-B+");
	//lSystem.AddProduction('B',"-A+BB++B+A--A-B");


	/* 
	// Polygon test #1
	LSystem lSystem(60.0f,{"F-F-F-F-F-F"});
	m_pMesh = lSystem.Run(0);
	*/



	/*
	// Polygon test #2
	LSystem lSystem(60.0f,{"F+F+F+F+F+F"});
	m_pMesh = lSystem.Run(0);
	*/
	
	
  	// Quadratic Gosper curve 
	LSystem lSystem(90.0f,{"-B"});
	lSystem.AddProduction('A',"AA-B-B+A+A-B-BA+B+AAB-A+B+AA+B-AB-B-A+A+BB-");
	lSystem.AddProduction('B',"+AA-B-B+A+AB+A-BB-A-B+ABB-A-BA+A+B-B-A+A+BB");
	m_pMesh = lSystem.Run(2);
	
	/*
  	// Hexagonal Gosper curve 
	LSystem lSystem(60.0f,{"A"});
	lSystem.AddProduction('A',"A+B++B-A--AA-B+");
	lSystem.AddProduction('B',"-A+BB++B+A--A-B");
	m_pMesh = lSystem.Run(5);*/
	
	/*
  	// Box test 
	LSystem lSystem(90.0f,{"F-F-F-F"});
	m_pMesh = lSystem.Run(0);
	*/

	/*
  	// Box test 2
	LSystem lSystem(90.0f,{"F+F+F+F"});
	m_pMesh = lSystem.Run(0);
	*/


	/*
  	// Sierpinski gasket
	LSystem lSystem(60.0f,{"A"});
	lSystem.AddProduction('A',"B-A-B");
	lSystem.AddProduction('B',"A+B+A");
	m_pMesh = lSystem.Run(8);*/
	
	/*
  	// Kotch curve #4
	LSystem lSystem(90.0f,{"F-F-F-F"});
	lSystem.AddProduction('F',"FF-F--F-F");
	m_pMesh = lSystem.Run(5);*/

	/*
  	// Dragon curve #4
	LSystem lSystem(90.0f,{"A"});
	lSystem.AddProduction('A',"A+B+");
	lSystem.AddProduction('B',"-A-B");
	m_pMesh = lSystem.Run(10);*/


	/*
	// Tree test #1
	LSystem lSystem(25.7f,{"F"});
	lSystem.AddProduction('F',"c2F[c1+F]F[c4-F]F");
	m_pMesh = lSystem.Run(5);*/


	/*
	// Tree test #2
	LSystem lSystem(22.5f,{"F"});
	lSystem.AddProduction('F',"c2FF-[c1-F+F+F]+[c4+F-F-F]");
	m_pMesh = lSystem.Run(1);
	*/


	/*
	// Cool floaty box thing
	LSystem lSystem(90.0f,{"c3F+F+F+F"});
	lSystem.AddProduction('F',"F+f-FF+F+FF+Ff+FF-f+FF-F-FF-Ff-FF");
	lSystem.AddProduction('f',"ffffff");
	
	m_pMesh = lSystem.Run(2);*/

	/*
	// Quadratic Koch island
	LSystem lSystem(90.0f,{"F-F-F-F"});
	lSystem.AddProduction('F',"F+FF-FF-F-F+F+FF-F-F+F+FF+FF-F");
	m_pMesh = lSystem.Run(2);*/


}


void Application::Render()
{
	m_pProgram->UseProgram();
	glm::mat4 MVP = m_Projection;

	// TODO: Move this.
	m_pProgram->SetUniformMatrix4fv("MVP",1,false,glm::value_ptr(MVP));

	m_pRenderer->Clear();
	m_pMesh->Render();
	m_pRenderer->SwapBuffers();
}


void Application::Run()
{
	bool running = true;
	while(running)
	{
		if (!m_pRenderer->CheckWindowEvents())
		{
			running = false;
		}

		Render();
	}
}