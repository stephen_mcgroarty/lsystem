#ifndef LSYSTEM_H
#define LSYSTEM_H
#include "Graphics/Mesh.h"
#include <map>
#include <string>
#include <stack>

class LSystem
{
public:
	LSystem(float angle,const std::string & axion);

	std::shared_ptr<Mesh> Run(int iterations);


	void AddProduction(char c,const std::string & str);
private:
	float m_Angle;
	float m_Step;
	std::string m_Axion;

	void ResolveGrammar(std::string & out,int) const;

	std::shared_ptr<Mesh> InterpretString(const std::string &) const;


	const char * TurtleInterpret(const char * cursor,
							glm::vec2 pos,
							glm::vec2 dir,
							glm::vec3 col,
							float heading,
							std::shared_ptr<Mesh> pMesh) const;
	std::map<char,std::string> m_Productions;

};

#endif