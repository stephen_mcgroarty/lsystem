#include "Graphics/Renderer.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>



Renderer::Renderer(int width,int height): m_Width(width), m_Height(height)
{
	SDL_Init(SDL_INIT_VIDEO);
	m_pWindow = SDL_CreateWindow("City Generator",
								SDL_WINDOWPOS_CENTERED,
								SDL_WINDOWPOS_CENTERED,
								m_Width,
								m_Height,
								SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE); 

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);  

	m_Context = SDL_GL_CreateContext(m_pWindow);

	if (!m_Context)
	{
		fprintf(stderr,"Error creating graphics context!\n");
	}


	glewExperimental = GL_TRUE;
	GLenum e = glewInit();
	if( e != GLEW_OK )
	{
		fprintf(stderr,"Problem with glew, failed init\n");
		exit(0);
	}


	// Sometimes glewInit throws an error for an unknown reason.
	// We report it but don't abort as it doesn't appear fatal.
	GLenum err = glGetError();
	if( err != GL_NO_ERROR )
	{
		fprintf(stderr,"Error: %d Thrown in glewInit\n",err);
	}


	SDL_GL_SetSwapInterval(1);
	//glEnable(GL_DEPTH_TEST);
	glClearColor(0.4f,0.4f,0.4f,1.0f);

}

Renderer::~Renderer()
{
	SDL_GL_DeleteContext(m_Context);
	SDL_DestroyWindow(m_pWindow);
	SDL_Quit();
}



bool Renderer::CheckWindowEvents() const
{
	SDL_Event runEvent;

	while( SDL_PollEvent(&runEvent) )
	{
		if ( runEvent.type == SDL_QUIT )
			return false;
	}
	return true;
}


void Renderer::Clear() const
{
	glClear( GL_COLOR_BUFFER_BIT);
}


void Renderer::SwapBuffers()
{
	SDL_GL_SwapWindow(m_pWindow);
}
