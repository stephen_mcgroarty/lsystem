#ifndef RENDERER_H
#define RENDERER_H

#include "Graphics/Mesh.h"
#include <SDL.h>
#include <GL/glew.h>
#include "Graphics/ShaderProgram.h"


class Renderer
{
public:
	Renderer(int width,int height);
	~Renderer();
	void RenderMesh(const Mesh & mesh) const;

	void Clear() const;

	void SwapBuffers();

	bool CheckWindowEvents() const;
private:
	int m_Width, m_Height;
	// Pointer memory managed by SDL
	SDL_Window * m_pWindow;

	SDL_GLContext m_Context;
};

#endif