#include "Graphics/ShaderProgram.h"
#include <fstream>
#include <stdio.h>


ShaderProgram::ShaderProgram(): m_ProgramID(0) { }


ShaderProgram::~ShaderProgram()
{
	glDeleteProgram(m_ProgramID);

	for (auto shader : m_Shaders)
	{
		glDeleteShader(shader);
	}
}


bool ShaderProgram::LoadShader(ShaderType type, const char * filepath)
{
	GLuint shaderID;
	if (type == ShaderType::VERTEX)
	{
		shaderID = glCreateShader(GL_VERTEX_SHADER);
	} 
	else if(type == ShaderType::FRAGMENT)
	{
		shaderID = glCreateShader(GL_FRAGMENT_SHADER);
	}

	GLenum err = glGetError();
	if( err != GL_NO_ERROR )
	{
		fprintf(stderr,"Error: %d while creating shader",err);
		int length;
		glGetShaderiv(shaderID,GL_INFO_LOG_LENGTH,&length);

		char * ch = new char[length];
		glGetShaderInfoLog(shaderID,length,NULL,ch);
		fprintf(stderr,"%s",ch);
		return false;
	}

	// Load the shader source
	std::ifstream source(filepath, std::ios::binary | std::ios::ate| std::ios::in);
	GLuint size = source.tellg();
	source.seekg(source.beg);

	char * str = new char[size+1];
	const char * block = str;
	source.read(str,size);
	source.close();
	str[size] = '\0';

	glShaderSource(shaderID,1,&block,NULL);


	// Compile the shader
	glCompileShader(shaderID);

	int comp;
	glGetShaderiv(shaderID,GL_COMPILE_STATUS,&comp);
	err = glGetError();
	if( err != GL_NO_ERROR || !comp )
	{
		fprintf(stderr,"Error: %d while compiling shader",err);
		int length;
		glGetShaderiv(shaderID,GL_INFO_LOG_LENGTH,&length);

		char * ch = new char[length];
		glGetShaderInfoLog(shaderID,length,NULL,ch);
		fprintf(stderr,"%s",ch);
		return false;
	}

	m_Shaders.push_back(shaderID);
	return true;
}


bool ShaderProgram::Finalize()
{
	m_ProgramID = glCreateProgram();

	for (GLuint shader : m_Shaders)
	{
		glAttachShader(m_ProgramID,shader);
	}
	glLinkProgram(m_ProgramID);

	GLenum err = glGetError();
	if( err != GL_NO_ERROR )
	{
		fprintf(stderr,"Error: %d while linking shader program",err);
		return false;
	}

	glUseProgram(m_ProgramID);
	return true;
}


void ShaderProgram::UseProgram() const
{
	glUseProgram(m_ProgramID);
}



GLint ShaderProgram::GetUniformLocation(const char * paramName) const 
{
	GLint loc = glGetUniformLocation(m_ProgramID,paramName);

	if( loc == -1)
	{
		fprintf(stderr,"No uniform found with name: %s\n",paramName);
		return -1;
	}

	return loc;
}



void ShaderProgram::SetUniform1i(const char * paramName,const int i) const
{
	glUniform1i(GetUniformLocation(paramName),i);
}

void ShaderProgram::SetUniform1f(const char * paramName,float f) const
{
	glUniform1f(GetUniformLocation(paramName),f);
}

void ShaderProgram::SetUniform2fv(const char * paramName,int count,float *f) const
{
	glUniform2fv(GetUniformLocation(paramName),count,f);
}

void ShaderProgram::SetUniform3fv(const char * paramName,int count,float *f) const
{
	glUniform3fv(GetUniformLocation(paramName),count,f);
}

void ShaderProgram::SetUniform4fv(const char * paramName,int count,float *f) const
{
	glUniform4fv(GetUniformLocation(paramName),count,f);
}

void ShaderProgram::SetUniformMatrix3fv(const char * paramName,int count,bool t,float * value) const
{
	glUniformMatrix4fv(GetUniformLocation(paramName),count,t,value);
}

void ShaderProgram::SetUniformMatrix4fv(const char * paramName,int count,bool t,float * value) const
{
	glUniformMatrix4fv(GetUniformLocation(paramName),count,t,value);
}

