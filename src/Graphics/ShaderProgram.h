#ifndef SHADER_PROGRAM_H
#define SHADER_PROGRAM_H
#include <GL/glew.h>
#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

class ShaderProgram
{
public:
	enum ShaderType: char
	{
		VERTEX,
		FRAGMENT
	};

	ShaderProgram();
	~ShaderProgram();

	bool LoadShader(ShaderType type, const char * filepath);

	bool Finalize();
	void UseProgram() const;


	GLint GetUniformLocation(const char * paramName) const;


	void SetUniform1i(const char * paramName,const int i) const;
	void SetUniform1f(const char * paramName,float f) const;
	void SetUniform2fv(const char * paramName,int count,float *f) const;
	void SetUniform3fv(const char * paramName,int count,float *f) const;
	void SetUniform4fv(const char * paramName,int count,float *f) const;
	void SetUniformMatrix3fv(const char * paramName,int count,bool t,float * value) const;
	void SetUniformMatrix4fv(const char * paramName,int count,bool t,float * value) const;

private:
	GLuint m_ProgramID;

	std::vector<GLuint> m_Shaders;

};


#endif