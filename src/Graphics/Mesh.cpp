#include <stdio.h>
#include "Graphics/Mesh.h"
#include <cmath>
#include <limits.h>
Mesh::Mesh()
{

}


Mesh::Mesh(const std::vector<float>& data,const std::vector<float>&  uv) 
{
	Finalize(data,uv);
	m_Size = data.size();
}
	

void Mesh::NormalizeToScreen()
{
	glm::vec2 top {-FLT_MAX,-FLT_MAX};
	glm::vec2 bottom {FLT_MAX,FLT_MAX};

	for (int index = 0; index < m_TempVertexBuffer.size(); index += 3)
	{
		if (top.x < m_TempVertexBuffer[index])
		{
			top.x = m_TempVertexBuffer[index];
		}

		if ( bottom.x > m_TempVertexBuffer[index] )
		{
			bottom.x = m_TempVertexBuffer[index];
		}

		if (top.y < m_TempVertexBuffer[index+1])
		{
			top.y = m_TempVertexBuffer[index+1];
		}

		if ( bottom.y > m_TempVertexBuffer[index+1])
		{
			bottom.y = m_TempVertexBuffer[index+1];
		}
	}



	glm::vec2 halfwayPoint {(bottom.x+top.x)/2,(bottom.y+top.y)/2};
	glm::vec2 size {sqrt(top.x*top.x) + sqrt(bottom.x*bottom.x),
							 sqrt(top.y*top.y) + sqrt(bottom.y*bottom.y)};

	printf("Top  (%f,%f)\n",top.x,top.y);
	printf("Bottom  (%f,%f)\n",bottom.x,bottom.y);
	printf("Halfway point: (%f,%f) \n",halfwayPoint.x,halfwayPoint.y);
	printf("Size: (%f,%f)\n",size.x,size.y);

	for (int index = 0; index < m_TempVertexBuffer.size(); index += 3)
	{
		m_TempVertexBuffer[index]  -= halfwayPoint.x;
		m_TempVertexBuffer[index+1]-= halfwayPoint.y;

		if (size.x != 0) m_TempVertexBuffer[index]   *= (0.9f/size.x); //* (size.x/size.y);
		if (size.y != 0) m_TempVertexBuffer[index+1] *= (0.9f/size.y); //* (size.x/size.y);
	}
}


void Mesh::Finalize()
{	
	printf("Finalizing mesh of size %lu\n",m_TempVertexBuffer.size());
	Finalize(m_TempVertexBuffer,m_TempUVBuffer);
}


void Mesh::Finalize(const std::vector<float>& data,const std::vector<float>& uv)
{
	glGenVertexArrays(1, &m_VertexArrayID);
	glBindVertexArray(m_VertexArrayID);

	GLenum err = glGetError();
	if( err != GL_NO_ERROR )
	{
		fprintf(stderr,"Error: %d Thrown in VertexArray creation\n",err);
	}



	glGenBuffers(1, &m_VertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, m_VertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, data.size()*sizeof(float), &data[0], GL_STATIC_DRAW);

	err = glGetError();
	if( err != GL_NO_ERROR )
	{
		fprintf(stderr,"Error: %d Thrown in VertexBuffer creation\n",err);
	}


	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, m_VertexBuffer);
	glVertexAttribPointer(0,3,GL_FLOAT, GL_FALSE,0,(void*)0);

	if (uv.size() > 0)
	{
		glGenBuffers(1, &m_UVBuffer);
		glBindBuffer(GL_ARRAY_BUFFER, m_UVBuffer);
		glBufferData(GL_ARRAY_BUFFER, uv.size()*sizeof(float), &(uv[0]), GL_STATIC_DRAW);
		glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, m_UVBuffer);
		glVertexAttribPointer(1,3,GL_FLOAT, GL_FALSE,0,(void*)0);

		err = glGetError();
		if( err != GL_NO_ERROR )
		{
			fprintf(stderr,"Error: %d Thrown in UV buffer creation\n",err);
		}		
	}
	
	m_Size = data.size();
}



void Mesh::AddVertice(const glm::vec2 & vertice)
{
	m_TempVertexBuffer.push_back(vertice.x);
	m_TempVertexBuffer.push_back(vertice.y);
	m_TempVertexBuffer.push_back(-1.0f);
}


void Mesh::AddColour(const glm::vec3 & colour)
{
	m_TempUVBuffer.push_back(colour.x);
	m_TempUVBuffer.push_back(colour.y);
	m_TempUVBuffer.push_back(colour.z);
	
}

Mesh::~Mesh()
{
	glDeleteVertexArrays(1,&m_VertexArrayID);
	glDeleteBuffers(1,&m_VertexBuffer);
}


void Mesh::Render() const
{
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, m_VertexBuffer);
	glDrawArrays(GL_LINES, 0, m_Size); 

	GLenum err = glGetError();
	if( err != GL_NO_ERROR )
	{
		fprintf(stderr,"Error: %d Thrown in glDrawArrays",err);
	}

	glDisableVertexAttribArray(0);
}
