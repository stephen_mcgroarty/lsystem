#ifndef MESH_H
#define MESH_H
#include <GL/glew.h>
#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

class Mesh
{
public:
	Mesh();

	Mesh(const std::vector<float>& data,const std::vector<float>& uv);

	~Mesh();
 

	// If you created with the empty contructor this is needed.
	// It takes the vertices and makes OpenGL buffers from them
	// other constructor does that already.
	void Finalize();

	void AddVertice(const glm::vec2 & vertice);
	void AddColour(const glm::vec3 & colour);

	void NormalizeToScreen();

	void Render() const;
private:

	GLuint m_VertexArrayID;
	GLuint m_VertexBuffer,m_UVBuffer;
	int m_Size;

	void Finalize(const std::vector<float>& data,const std::vector<float>& uv);


	// These are converted into OpenGL buffers and
	// cleared when the mesh is finalized.
	std::vector<float> m_TempVertexBuffer;
	std::vector<float> m_TempUVBuffer;
};

#endif