#ifndef APPLICATION_H
#define APPLICATION_H
#include "Graphics/Renderer.h"
#include "Graphics/Mesh.h"
class Application
{
public:

	Application();

	void Run();

private:
	std::unique_ptr<Renderer> m_pRenderer;
	std::shared_ptr<ShaderProgram> m_pProgram;
	std::shared_ptr<Mesh> m_pMesh;
	glm::mat4 m_View;
	glm::mat4 m_Projection;
	void Render();

};

#endif