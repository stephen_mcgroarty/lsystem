CMAKE_MINIMUM_REQUIRED(VERSION 2.8)
project(LSystem C CXX)

set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake_modules" ${CMAKE_MODULE_PATH})



file(GLOB_RECURSE SOURCE "${CMAKE_CURRENT_SOURCE_DIR}/src/*")


find_package(SDL2 REQUIRED)
find_package(GLEW REQUIRED)
find_package(OpenGL REQUIRED)
find_package(GLM REQUIRED)




set(CMAKE_CXX_FLAGS "-std=c++11 -Wall -Werror") 

message(STATUS "GLM include dir: ${GLM_INCLUDE_DIR}")
message(STATUS "GLEW include dir: ${GLEW_INCLUDE_DIR}")

include_directories(${SDL2_INCLUDE_DIR} "${CMAKE_CURRENT_SOURCE_DIR}/src" ${GLEW_INCLUDE_DIR} ${GLM_INCLUDE_DIRS})

add_executable(LSystem ${SOURCE})


message(STATUS "OPENGL: ${OPENGL_LIBRARIES}")
target_link_libraries(LSystem ${SDL2_LIBRARY} ${GLEW_LIBRARIES} ${OPENGL_LIBRARIES})
