#version 410 core

layout(location = 0) in vec4 model;
layout(location = 1) in vec3 colour;

out vec3 out_Colour;

uniform mat4 MVP;
void main()
{
   gl_Position= MVP*model;
   out_Colour =colour;
}