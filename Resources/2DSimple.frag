#version 410 core

in vec3 out_Colour;
out vec3 colour;

void main()
{
    colour = out_Colour;
}